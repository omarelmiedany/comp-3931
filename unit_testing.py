import pytest

import chatbot
import query

def test_subject_extractor():
    session = chatbot.ChatSession()
    test1 = session.subject_extractor("Where can I find fruity")
    assert test1[0]=='fruity'
    test2 = session.subject_extractor("What time is ballroom and latin training")
    assert test2[0]=='ballroom'
    test3 = session.subject_extractor("Is there an archery society")
    assert test3[0]=='archery'

def test_general_query():
    db_query = query.QueryHandler()
    test1 = db_query.general_query("Dance", "ballroom")
    assert str(test1[0][0])=="Dancesport"
    test2 = db_query.general_query("Dance", ["ballroom","latin"])
    assert str(test2[0][0]) == "Dancesport"
    test3 = db_query.general_query("Social/Sport", "football")
    assert test3
    test4 = db_query.general_query("Alcohol_Free","")
    assert test4[0][0]=="High on Life"
    test5 = db_query.general_query("","")
    assert test5

def test_is_activity():
    db_query = query.QueryHandler()
    test1 = db_query.isActivity("tropicana")
    print()
    assert test1
    test2 = db_query.isActivity("ball")
    assert not bool(test2)