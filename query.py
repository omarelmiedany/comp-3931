import sqlite3
import random
from random import shuffle
class QueryHandler:
    def __init__(self):
        self.database = self.db_connect(r"database/recommender_db")

    def db_connect(self,db_file):
        connection = None
        try:
            connection = sqlite3.connect(db_file)
        except Error as e:
            print(e)
        return connection

    def quicksort(self,list):

        less = []
        equal = []
        greater = []

        if len(list) > 1:
            pivot = list[0][4]
            for x in list:
                if x[4] < pivot:
                    less.append(x)
                elif x[4] == pivot:
                    equal.append(x)
                elif x[4] > pivot:
                    greater.append(x)

            return self.quicksort(greater)+equal+self.quicksort(less)

        else:
            return list

    def isActivity(self, words):
        text = ""
        if isinstance(words, list):
            for i in words:
                text = text + " Title LIKE '%"
                text = text + i
                text = text + "%' OR"

            text = text[:-3]
        else:
            text = " Title LIKE '%"+words+"%'"

        cursor = self.database.cursor()
        query = "SELECT * FROM Activities WHERE"
        query = query+text
        cursor.execute(query)

        entities = cursor.fetchall()
        #only returns activity if there's one
        if len(entities)==1:
            return entities
        else:
            return None

    #calculates recommendations using interest model
    def query_by_intrests(self,interests):
        #sql query
        text = ""
        if isinstance(interests, list):
            for i in interests:
                text = text+" Tags LIKE '%"
                text = text+i
                text = text+"%' OR"

            text = text[:-3]
        else:
            text = " Tags LIKE '%"+interests+"%'"

        cursor = self.database.cursor()
        query = "SELECT * FROM Activities WHERE"
        query = query+text
        cursor.execute(query)

        entities = cursor.fetchall()
        ranked_entities = []

	#calculates score for each activity
        for entity in entities:
            tags = entity[4].split('/')
            tf = 0#tag frequency
            for i in interests:
                for j in tags:
                    if i==j:
                        tf = tf + 1

            score = tf/len(interests)
            ranked_entities.append((entity[0],entity[1],entity[2],entity[3],score))

        random.shuffle(ranked_entities)
        ranked_entities = self.quicksort(ranked_entities)
        return ranked_entities

    #finds all activities of a certain type using keywords
    def query_by_description(self,keywords):
        text = ""
        if isinstance(keywords, list):
            for i in keywords:
                text = text + " Description LIKE '%"
                text = text + i
                text = text + "%' OR"

            text = text[:-3]
        else:
            text = " Description LIKE '%"+keywords+"%'"

        cursor = self.database.cursor()
        query = "SELECT * FROM Activities WHERE"
        query = query + text
        cursor.execute(query)
        entities = cursor.fetchall()

        return entities

    #random recommendations
    def query_all(self):
        cursor = self.database.cursor()
        query = "SELECT * FROM Activities"
        cursor.execute(query)
        entities = cursor.fetchall()
        random.shuffle(entities)

        return entities

    
    def general_query(self,interests,keywords):
        results = []

        if len(keywords)>0:
            description_results = self.query_by_description(keywords)
            if len(description_results)>2:
                results.extend(description_results[0:1])
            else:
                results.extend(description_results)

        if len(interests)>0:
            interest_results = self.query_by_intrests(interests)
            for i in results:
                if i in interest_results:
                    interest_results.remove(i)
            results.extend(interest_results)

        #only called if no recommendations have been made
        if len(results)==0:
            random_activities = self.query_all()
            results.extend(random_activities)

        return results[0:3]
