import numpy as np
import query
from random import choice
#sqlite
import sqlite3
from sqlite3 import Error
#natural language processing
from nltk.tokenize import word_tokenize
from nltk.chunk.regexp import *
from nltk import pos_tag
from nltk.stem import wordnet
#word2vector
from gensim.models import KeyedVectors
#tensorflow
from tensorflow.keras.models import load_model

#stemmer
stemmer = wordnet.WordNetLemmatizer()

vector_map = KeyedVectors.load("Classifiers/cnn_model/word2vec.wordvectors", mmap='r')
#labels
interests_labels = ["Alcohol_Free","Artful","Casual","Dance","Entertainment","Food+Drink","Free","Get_Active","Indoors","Night_Out","Outdoors","Relaxing","Self-Defence","Social","Solo","Sport","Sustainability","Trips","Volunteering","Wellbeing"]
intents_labels = ["Farewell","Greeting","Help","Query","Recommend"]
question_labels = ["Similar","What","When","Where"]
#classifiers
interests_classifier = load_model('Classifiers/cnn_model/InterestsModel.h5')
intents_classifier = load_model('Classifiers/cnn_model/IntentsModel.h5')
question_classifier = load_model('Classifiers/cnn_model/QuestionsModel.h5')
#flagged words
flagged_words = ["society","soc","try","restuarant","trip","time","dance","new","meet","dancing","casual","class","start","societies","club","venue","event","sport","find","i","me","you","he","she","it","classes","lessons","training","held","play","something","fun"]
accepted_pos_tags = ["NN","JJ","VBN"]

class ChatSession:
    def __init__(self):
        self.interests = []

	#converts sentence to list of vectors
    def sentence2vector (self,sentence):
        tokenized_sentence = word_tokenize(sentence.lower())
        cbow = np.zeros((25,100),dtype=np.float32)
        counter = 0
        for word in tokenized_sentence:
            try:
                vector = vector_map[word]
                for i in range(100):
                    cbow[counter][i]=vector.item(i)
                counter = counter + 1
            except KeyError:
                None

        return cbow

	#extracts nouns for the keywords
    def subject_extractor(self, sentence):
        tagged_sentence = pos_tag(sentence.split())
        #print(tagged_sentence)
        for word in tagged_sentence:
            if word[0] in flagged_words:
                tagged_sentence.remove(word)

        nouns = []
        index = 0
        while (index < len(tagged_sentence)):
            if tagged_sentence[index][1] in accepted_pos_tags:
                if index >= len(tagged_sentence) - 1:
                    nouns.append(tagged_sentence[index][0])
                elif tagged_sentence[index+1][1] == "NN":
                    nouns.append(tagged_sentence[index][0] + " " + tagged_sentence[index + 1][0])
                    index = index + 1
                else:
                    nouns.append(tagged_sentence[index][0])

            index = index + 1
            #stems word to mitigate for plurals
        nouns = [stemmer.lemmatize(word) for word in nouns]
        return nouns
        
        #calls interest classifier
    def predict_interests(self,user_input):
        probability = interests_classifier.predict(np.array(self.sentence2vector(user_input)).reshape(-1, 25, 100, 1))

        predicted_interests = []
        for i in range(20):
            if probability.item(i) > 0.6:
                predicted_interests.append(interests_labels[i])

        return predicted_interests

    	#updates user model
    def record_interests(self,predicted_interests):
        for i in predicted_interests:
            if i not in self.interests:
                self.interests.append(i)
        return None

	#if an acitivity is refrenced the question is classified and response calculated
    def specified_query(self,user_input,activity):
        probability = question_classifier.predict(np.array(self.sentence2vector(user_input)).reshape(-1, 25, 100, 1))
        response = ""

        #Similar
        if probability.item(0) > 0.6:
            response = self.general_query(activity[0][4].split('/'), "")
        #When
        elif probability.item(2) > 0.6:
            response = response + str(activity[0][0])+" is on "+str(activity[0][2])
        #Where
        elif probability.item(3) > 0.6:
            response = response + str(activity[0][0])+" can be found at "+str(activity[0][3])
        #What
        else:
            response = response + str(activity[0][0])+" is "+str(activity[0][1])

        return response

	#recommendations are calculated and combined with keyword search
    def general_query(self,predicted_interests,searchable_entities):
        self.record_interests(predicted_interests)
        db_query = query.QueryHandler()
        results = db_query.general_query(predicted_interests,searchable_entities)

        response = ["I think you would like:\n", "I would recommend:\n", "I think you should try\n"]
        recommendations = ""

        for entities in results:
            recommendations = recommendations+"• "+str(entities[0])+"<br>"

        if len(results)==0:
            return "Sorry I couldn't find anything"
        else:
            return choice(response)+"<br>"+recommendations

   	#responds with a random greeting
    def greeting(self):
        response = ["Hi!","Nice to meet you!","Hello :)"]
        return choice(response)

    	#responds with random farewell
    def farewell(self):
        response = ["Happy to help!","Always here to help :)","Here whenever you need me!","Happy I could help :)"]
        return choice(response)

    	#responds with how to use the chatbot
    def help(self):
        response = "I'm a chatbot that can help you discover Leeds University Union.\n" \
                "Just tell me what you're interested in and I can recommend you something!\n" \
                "I can also answer general questions on activities happening at the Union :)"
        return response

    	#calculates recommendations just with user model
    def recommend(self):
        if len(self.interests)==0:
            response = "Tell me what you're interested in"
        else:
            response = self.general_query(self.interests, "")
        return response

    	#checks if actiivity is refrenced to then decide on how to respond
    def query(self, user_input):
        searchable_entities = self.subject_extractor(user_input)
        db_query = query.QueryHandler()
        if searchable_entities:
            activity = db_query.isActivity(searchable_entities)
            if activity:
                response = self.specified_query(user_input,activity)
            else:
                predicted_interests = self.predict_interests(user_input)
                response = self.general_query(predicted_interests,searchable_entities)
        else:
            predicted_interests = self.predict_interests(user_input)
            response = self.general_query(predicted_interests, searchable_entities)

        return response

	#classifies the user's intent and calls the corresponding function
    def response_calculator(self,user_input):
        intent_probability = intents_classifier.predict(np.array(self.sentence2vector(user_input.lower())).reshape(-1,25,100,1))
        #Farewell
        if intent_probability.item(0)>0.6:
            response = self.farewell()
        #Greeting
        elif intent_probability.item(1)>0.6:
            response = self.greeting()
        #Help
        elif intent_probability.item(2)>0.6:
            response = self.help()
        #Query
        elif intent_probability.item(3)>0.6:
            response = self.query(user_input.lower())
        #Recommend
        elif intent_probability.item(4)>0.6:
            response = self.recommend()
        else:
            response = "Sorry I didn't seem to understand that"

        return response
