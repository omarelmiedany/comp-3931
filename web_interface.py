from flask import Flask, render_template, request
import os
import chatbot

app = Flask(__name__, template_folder='template')
chat_session = chatbot.ChatSession()

@app.route('/')
def chat():
    return render_template('base.html')

@app.route('/request')
def chat_request():
    userInput = request.args.get('message')
    return chat_session.response_calculator(userInput)

app.run(debug=True, port=5000)