import numpy as np
#nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
#word2vector
from gensim.models import KeyedVectors
#tensorflow
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint
#scikit learn
#csv
import csv
import random
#import time

vector_map = KeyedVectors.load("word2vec.wordvectors", mmap='r')
training_corpus = []
x_train = []
y_train = []
x_val = []
y_val = []

def sentence2vector (sentence):
    tokenized_sentence = word_tokenize(sentence.lower())

    cbow = np.zeros((25,100),dtype=np.float32)

    counter = 0
    for word in tokenized_sentence:
        try:
            vector = vector_map[word]
            for i in range(100):
                cbow[counter][i]=vector.item(i)
            counter = counter + 1
        except KeyError:
            None

    return cbow

with open('../training_data/interests_training_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        training_corpus.append((row[0],row[1]))

random.shuffle(training_corpus)

for i in training_corpus:
    x_train.append(sentence2vector(i[0]))
    y_train.append(i[1])

def multi_label_binarizer(str_labels):
    y_ml = []
    for i in str_labels:
        multi_label_binary=np.zeros(20)
        labels = i.split('/')
        for j in labels:
            if j == 'Alcohol_Free':
                multi_label_binary[0] = 1
            elif j == 'Artful':
                multi_label_binary[1] = 1
            elif j == 'Casual':
                multi_label_binary[2] = 1
            elif j == 'Dance':
                multi_label_binary[3] = 1
            elif j == 'Entertainment':
                multi_label_binary[4] = 1
            elif j == 'Food+Drink':
                multi_label_binary[5] = 1
            elif j == 'Free':
                multi_label_binary[6] = 1
            elif j == 'Get_Active':
                multi_label_binary[7] = 1
            elif j == 'Indoors':
                multi_label_binary[8] = 1
            elif j == 'Night_Out':
                multi_label_binary[9] = 1
            elif j == 'Outdoors':
                multi_label_binary[10] = 1
            elif j == 'Relaxing':
                multi_label_binary[11] = 1
            elif j == 'Self-Defence':
                multi_label_binary[12] = 1
            elif j == 'Social':
                multi_label_binary[13] = 1
            elif j == 'Solo':
                multi_label_binary[14] = 1
            elif j == 'Sport':
                multi_label_binary[15] = 1
            elif j == 'Sustainability':
                multi_label_binary[16] = 1
            elif j == 'Trips':
                multi_label_binary[17] = 1
            elif j == 'Volnteering':
                multi_label_binary[18] = 1
            elif j == 'Wellbeing':
                multi_label_binary[19] = 1
        y_ml.append(multi_label_binary)
    return y_ml

x_train = np.array(x_train).reshape(-1,25,100,1)
y_train_ml = np.array(multi_label_binarizer(y_train))

model = keras.Sequential (name="model")
model.add(keras.Input(shape=(25,100),name='input'))
model.add(layers.Dropout(0.2, input_shape=(25,100)))
model.add(layers.Conv1D(256,3,activation="relu",padding='valid'))
model.add(layers.MaxPool1D(pool_size=2,strides=2, padding='valid'))
model.add(layers.Conv1D(128,3,activation="relu",padding='valid'))
model.add(layers.MaxPool1D(pool_size=2,strides=2, padding='valid'))
model.add(layers.Flatten())
model.add(keras.layers.Dense(128, activation = 'sigmoid',name='hidden_layer'))
model.add(keras.layers.Dense(64, activation = 'sigmoid',name='hidden_layer2'))
model.add(keras.layers.Dense(20, activation = 'sigmoid',name='output'))
#print(model.summary())
#import sys; sys.exit()


model.compile(loss="BinaryFocalCrossentropy", optimizer='Adam',metrics=["accuracy"])
es = EarlyStopping(monitor='val_accuracy', mode='max', patience=50)
mc = ModelCheckpoint('InterestsModel2.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
model.fit(x_train,y_train_ml, epochs=500,batch_size=25, verbose=2, validation_split=0.2,callbacks=[es,mc])

