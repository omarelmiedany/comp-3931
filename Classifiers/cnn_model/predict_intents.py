import numpy as np
#nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
#word2vector
from gensim.models import KeyedVectors
#tensorflow
import tensorflow as tf
from tensorflow.keras.models import load_model
#scikit learn
from sklearn.preprocessing import MultiLabelBinarizer
#csv
import csv
import time

vector_map = KeyedVectors.load("word2vec.wordvectors", mmap='r')

def sentence2vector (sentence):
    tokenized_sentence = word_tokenize(sentence.lower())
    cbow = np.zeros((25,100),dtype=np.float32)

    counter = 0
    for word in tokenized_sentence:
        try:
            vector = vector_map[word]
            for i in range(100):
                cbow[counter][i]=vector.item(i)
            counter = counter + 1
        except KeyError:
            None

    return cbow

interests_model = load_model('IntentsModel.h5')

while (True):
    test = input()
    test = np.array(sentence2vector(test)).reshape(-1,25,100,1)
    probability = interests_model.predict(test)

    labels = "General","Goodbye","Greeting","Help","More Info","More Recommendations","Similar","When","Where"
    for i in range(9):
        classification = 0
        if probability.item(i)>0.5:
            classification = 1
        print(labels[i],": ",classification)



