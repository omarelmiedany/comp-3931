import numpy as np
#nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
#word2vector
from gensim.models import KeyedVectors
#tensorflow
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint
#scikit learn
from sklearn.preprocessing import MultiLabelBinarizer
#csv
import csv
import random
#import time

vector_map = KeyedVectors.load("word2vec.wordvectors", mmap='r')
training_corpus = []
x_train = []
y_train = []

def sentence2vector (sentence):
    tokenized_sentence = word_tokenize(sentence.lower())

    cbow = np.zeros((25,100),dtype=np.float32)

    counter = 0
    for word in tokenized_sentence:
        try:
            vector = vector_map[word]
            for i in range(100):
                cbow[counter][i]=vector.item(i)
            counter = counter + 1
        except KeyError:
            None

    return cbow

with open('../training_data/questions_training_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        training_corpus.append((row[0],row[1]))

random.shuffle(training_corpus)

for i in training_corpus:
    x_train.append(sentence2vector(i[0]))
    y_train.append(i[1].split('/'))

mlb = MultiLabelBinarizer()
y_train_ml = mlb.fit_transform(y_train)


x_train = np.array(x_train).reshape(-1,25,100,1)

model = keras.Sequential (name="model")
model.add(keras.Input(shape=(25,100),name='input'))
model.add(layers.Conv1D(32,3,activation="relu",padding="valid"))
model.add(layers.MaxPool1D(pool_size=2,strides=2, padding='valid'))
model.add(layers.Conv1D(16, 3, activation='relu'))
model.add(layers.MaxPool1D(pool_size=2,strides=2, padding='valid'))
model.add(layers.Flatten())
model.add(keras.layers.Dense(16, activation = 'sigmoid',name='hidden_layer'))
model.add(keras.layers.Dense(4, activation = 'sigmoid',name='output'))
#print(model.summary())
#import sys; sys.exit()


model.compile(loss="binary_crossentropy", optimizer='Adam',metrics=["accuracy"])
es = EarlyStopping(monitor='val_accuracy', mode='max', patience=10)
mc = ModelCheckpoint('QuestionsModel2.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
model.fit(x_train,y_train_ml, epochs=100,batch_size=1, verbose=2, validation_split=0.2,callbacks=[es,mc])

