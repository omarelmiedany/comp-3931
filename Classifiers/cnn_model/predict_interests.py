import numpy as np
#nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
#word2vector
from gensim.models import KeyedVectors
#tensorflow
import tensorflow as tf
from tensorflow.keras.models import load_model
#scikit learn
from sklearn.preprocessing import MultiLabelBinarizer
#csv
import csv
import time

vector_map = KeyedVectors.load("word2vec.wordvectors", mmap='r')

def sentence2vector (sentence):
    tokenized_sentence = word_tokenize(sentence.lower())
    #try with and without stopwords
    '''
    for word in tokenized_sentence:
        if word in stopwords.words('english'):
            tokenized_sentence.remove(word)
    '''
    cbow = np.zeros((25,100),dtype=np.float32)

    counter = 0
    for word in tokenized_sentence:
        try:
            vector = vector_map[word]
            for i in range(100):
                cbow[counter][i]=vector.item(i)
            counter = counter + 1
        except KeyError:
            None

    return cbow

interests_model = load_model('InterestsModel2.h5')

while (True):
    test = input()
    test = np.array(sentence2vector(test)).reshape(-1,25,100,1)
    probability = interests_model.predict(test)

    labels = "ALcohol Free","Artful","Casual","Dance","Entertainment","Food+Drink","Free","Get_Active","Indoors","Night_Out","Outdoors","Relaxing","Self-Defence","Social","Solo","Sport","Sustainability","Trips","Volunteering","Wellbeing"
    str = ""
    for i in range(20):
        classification = 0
        if probability.item(i)>0.6:
            str = str+labels[i]
            str = str+"/"
            
    print(str)



