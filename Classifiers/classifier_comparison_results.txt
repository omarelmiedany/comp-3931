Intent Classifiers
              Accuracy     Recall
CNN:           87.88       86.67
Logistic Reg:  78.79       76.67
Linear SVC:    81.82       80.00
SVM:           69.70       66.67
MLP:           75.76       73.33

Interests Classifiers
              Accuracy     Recall
CNN:           86.32       83.65
Logistic Reg:  70.53       66.13
Linear SVC:    83.16       80.04
SVM:           65.26       69.33
MLP:           75.79       71.63

Question Classifiers
              Accuracy     Recall
CNN:           94.12       95.00
Logistic Reg:  94.12       93.75
Linear SVC:    94.12       93.75
SVM:           70.59       70.00
MLP:           94.12       93.75

