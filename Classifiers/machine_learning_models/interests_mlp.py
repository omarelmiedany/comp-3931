import numpy as np
import random
#natural language processing
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
#multi-layer perceptron
from sklearn.neural_network import MLPClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.pipeline import Pipeline
#file
import csv
import pickle

sb = SnowballStemmer(language='english')

#all_interests = []
train_corpus = []
test_corpus = []
all_words = []
x_train = []
y_train = []
x_test = []
y_test = []

with open('../training_data/interests_training_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        tokenized_sentence = word_tokenize(row[0].lower())
        tokenized_sentence = [sb.stem(word) for word in tokenized_sentence]

        for word in tokenized_sentence:
            if word not in all_words:
                all_words.append(word)

        train_corpus.append((row[0],row[1]))

random.shuffle(train_corpus)

with open('../training_data/interests_testing_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        test_corpus.append(row[0].lower())
        y_test.append((row[1].split("/")))

def featurise(sentence):
    bag_of_words = np.zeros(len(all_words), dtype=np.float32)
    tokenized_sentence = word_tokenize(sentence.lower())
    tokenized_sentence = [sb.stem(word) for word in tokenized_sentence]
    for index, word in enumerate(all_words):
        if (word in tokenized_sentence):
            bag_of_words[index] = 1

    return bag_of_words


for i in train_corpus:
    x_train.append(featurise(i[0]))
    y_train.append(i[1].split("/"))


for i in test_corpus:
    x_test.append(featurise(i))


mlb = MultiLabelBinarizer()
y_train_ml = mlb.fit_transform(y_train)
y_test_ml = mlb.fit_transform(y_test)


mlp = OneVsRestClassifier(MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(235,117), random_state=1))

mlp.fit(x_train, y_train_ml)

accuracy = mlp.score(x_test,y_test_ml)

print("Accuracy: ",accuracy)
example = featurise("What team sports do you have").reshape(1, -1)
pickle.dump(mlp, open('saved_models/interests_mlp.sav', 'wb'))

