import numpy as np
#nltk
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
#word2vector
from gensim.models import KeyedVectors
#tensorflow
from tensorflow.keras.models import load_model
#scikit learn
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import classification_report
#csv
import csv
import pickle

sb = SnowballStemmer(language='english')

vector_map = KeyedVectors.load("cnn_model/word2vec.wordvectors", mmap='r')
all_words = pickle.load(open("machine_learning_models/saved_models/vocabulary.sav", 'rb'))
labels = ["Alcohol_Free","Artful","Casual","Dance","Entertainment","Food+Drink","Free","Get_Active","Indoors","Night_Out","Outdoors","Relaxing","Self-Defence","Social","Solo","Sport","Sustainability","Trips","Volunteering","Wellbeing"]
intents_labels = ["Farewell","Greeting","Help","Query","Recommend"]
questions_labels = ["Similar","What","When","Where"]

interests_cnn = load_model('cnn_model/InterestsModel.h5')
intents_cnn = load_model('cnn_model/IntentsModel.h5')
questions_cnn = load_model('cnn_model/QuestionsModel2.h5')

interests_linear_SVC = pickle.load(open("machine_learning_models/saved_models/interests_linearSVC.sav", 'rb'))
intents_linear_SVC = pickle.load(open("machine_learning_models/saved_models/intents_linearSVC.sav", 'rb'))
questions_linear_SVC = pickle.load(open("machine_learning_models/saved_models/questions_linearSVC.sav", 'rb'))

interests_logistic_regression = pickle.load(open("machine_learning_models/saved_models/interests_logistic_regression.sav", 'rb'))
intents_logistic_regression = pickle.load(open("machine_learning_models/saved_models/intents_logistic_regression.sav", 'rb'))
questions_logistic_regression = pickle.load(open("machine_learning_models/saved_models/questions_logistic_regression.sav", 'rb'))

interests_mlp = pickle.load(open("machine_learning_models/saved_models/interests_mlp.sav", 'rb'))
intents_mlp = pickle.load(open("machine_learning_models/saved_models/intents_mlp.sav", 'rb'))
questions_mlp = pickle.load(open("machine_learning_models/saved_models/questions_mlp.sav", 'rb'))

interests_svm = pickle.load(open("machine_learning_models/saved_models/interests_svm.sav", 'rb'))
intents_svm = pickle.load(open("machine_learning_models/saved_models/intents_svm.sav", 'rb'))
questions_svm = pickle.load(open("machine_learning_models/saved_models/questions_svm.sav", 'rb'))

interests_x_test = []
interests_y_test = []
intents_x_test = []
intents_y_test = []
questions_x_test = []
questions_y_test = []
raw_interests_y_test = []
raw_intents_y_test = []

def sentence2vector (sentence):
    tokenized_sentence = word_tokenize(sentence.lower())
    cbow = np.zeros((25,100),dtype=np.float32)

    counter = 0
    for word in tokenized_sentence:
        try:
            vector = vector_map[word]
            for i in range(100):
                cbow[counter][i]=vector.item(i)
            counter = counter + 1
        except KeyError:
            None

    return cbow

def featurise(sentence):
    bag_of_words = np.zeros(len(all_words), dtype=np.float32)
    tokenized_sentence = word_tokenize(sentence.lower())
    tokenized_sentence = [sb.stem(word) for word in tokenized_sentence]
    for index, word in enumerate(all_words):
        if (word in tokenized_sentence):
            bag_of_words[index] = 1

    return bag_of_words

with open('training_data/interests_testing_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        interests_x_test.append(row[0].lower())
        interests_y_test.append((row[1].split("/")))
        raw_interests_y_test.append(row[1])

with open('training_data/intents_testing_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        intents_x_test.append(row[0].lower())
        intents_y_test.append((row[1].split("/")))
        raw_intents_y_test.append(row[1])

with open('training_data/questions_testing_data.csv') as csv_file:
    reader = csv.reader(csv_file, delimiter=',')
    for row in reader:
        questions_x_test.append(row[0].lower())
        questions_y_test.append((row[1].split("/")))

mlb = MultiLabelBinarizer()
interests_mlb = mlb.fit_transform(interests_y_test)
interests_y_true = np.argmax(interests_mlb, axis=1)
intents_mlb = mlb.fit_transform(intents_y_test)
intents_y_true = np.argmax(intents_mlb, axis=1)
questions_mlb = mlb.fit_transform(questions_y_test)
questions_y_true = np.argmax(questions_mlb, axis=1)

def report_calculator(y_true,y_pred):
    score = []
    score.append(accuracy_score(y_true,y_pred))#accuracy
    score.append(recall_score(y_true,y_pred, average='macro'))#recall
    return score

def interests_featuriser(model):
    x_test_featurised = []
    for i in interests_x_test:
        x_test_featurised.append(featurise(i))

    y_pred = np.argmax(model.predict(x_test_featurised), axis=1)

    return y_pred

def intents_featuriser(model):
    x_test_featurised = []
    for i in intents_x_test:
        x_test_featurised.append(featurise(i))

    y_pred = np.argmax(model.predict(x_test_featurised), axis=1)

    return y_pred

def questions_featuriser(model):
    x_test_featurised = []
    for i in questions_x_test:
        x_test_featurised.append(featurise(i))

    y_pred = np.argmax(model.predict(x_test_featurised), axis=1)

    return y_pred

def interests_vectoriser(model):
    x_test_vectorised = []
    for i in interests_x_test:
        x_test_vectorised.append(sentence2vector(i))

    x_test_vectorised = np.array(x_test_vectorised).reshape(-1,25,100,1)
    y_pred = model.predict(x_test_vectorised)
    for i in range(len(interests_x_test)):
        for j in range(20):
            if y_pred[i][j]>0.6:
                y_pred [i][j]=1
            else:
                y_pred[i][j]=0;

    y_pred = np.argmax(y_pred, axis=1)

    return y_pred

def intents_vectoriser(model):
    x_test_vectorised = []
    for i in intents_x_test:
        x_test_vectorised.append(sentence2vector(i))

    x_test_vectorised = np.array(x_test_vectorised).reshape(-1, 25, 100, 1)
    y_pred = model.predict(x_test_vectorised)
    for i in range(len(intents_x_test)):
        for j in range(4):
            if y_pred[i][j] > 0.6:
                y_pred[i][j] = 1
            else:
                y_pred[i][j] = 0;
    y_pred = np.argmax(y_pred, axis=1)

    return y_pred

def questions_vectoriser(model):
    x_test_vectorised = []
    for i in questions_x_test:
        x_test_vectorised.append(sentence2vector(i))

    x_test_vectorised = np.array(x_test_vectorised).reshape(-1, 25, 100, 1)
    y_pred = model.predict(x_test_vectorised)
    for i in range(len(questions_x_test)):
        for j in range(4):
            if y_pred[i][j] > 0.6:
                y_pred[i][j] = 1
            else:
                y_pred[i][j] = 0;
    y_pred = np.argmax(y_pred, axis=1)

    return y_pred

def interests_report():
    y_pred = interests_vectoriser(interests_cnn)
    print("              Accuracy     Recall")

    response = report_calculator(interests_y_true,y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("CNN:          ",formatted_response[0],"     ",formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = interests_featuriser(interests_logistic_regression)
    response = report_calculator(interests_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("Logistic Reg: ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = interests_featuriser(interests_linear_SVC)
    response = report_calculator(interests_y_true,y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("Linear SVC:   ",formatted_response[0], "     ",formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = interests_featuriser(interests_svm)
    response = report_calculator(interests_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("SVM:          ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = interests_featuriser(interests_mlp)
    response = report_calculator(interests_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("MLP:          ", formatted_response[0], "     ", formatted_response[1])

    return None
    
def intents_report():
    y_pred = intents_vectoriser(intents_cnn)
    print("              Accuracy     Recall")

    response = report_calculator(intents_y_true,y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("CNN:          ",formatted_response[0],"     ",formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = intents_featuriser(intents_logistic_regression)
    response = report_calculator(intents_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("Logistic Reg: ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = intents_featuriser(intents_linear_SVC)
    response = report_calculator(intents_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("Linear SVC:   ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = intents_featuriser(intents_svm)
    response = report_calculator(intents_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("SVM:          ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = intents_featuriser(intents_mlp)
    response = report_calculator(intents_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("MLP:          ", formatted_response[0], "     ", formatted_response[1])

    return None

def questions_report():
    y_pred = questions_vectoriser(questions_cnn)
    print("              Accuracy     Recall")

    response = report_calculator(questions_y_true,y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("CNN:          ",formatted_response[0],"     ",formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = questions_featuriser(questions_logistic_regression)
    response = report_calculator(questions_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("Logistic Reg: ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = questions_featuriser(questions_linear_SVC)
    response = report_calculator(questions_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("Linear SVC:   ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = questions_featuriser(questions_svm)
    response = report_calculator(questions_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("SVM:          ", formatted_response[0], "     ", formatted_response[1])

    response.clear()
    formatted_response.clear()
    y_pred = questions_featuriser(questions_mlp)
    response = report_calculator(questions_y_true, y_pred)
    formatted_response = [i * 100 for i in response]
    formatted_response = ['%.2f' % i for i in formatted_response]
    print("MLP:          ", formatted_response[0], "     ", formatted_response[1])
    return None

print("\nIntent Classifiers")
intents_report()
print("\nInterests Classifiers")
interests_report()
print("\nQuestion Classifiers")
questions_report()


#y_pred = intents_vectoriser(intents_cnn)
#print(classification_report(intents_y_true, y_pred, target_names=intents_labels))
#print(confusion_matrix(interests_y_true, y_pred))
